FROM artixlinux/artixlinux:latest AS build

WORKDIR /usr/artixlinux-docker
RUN pacman -Syu --noconfirm && \
  pacman -Sy --noconfirm artools curl make fakechroot fakeroot
ARG img=base-devel
COPY . .
RUN make "dockerfile-${img}"
WORKDIR /usr/artixlinux-docker/output
COPY . .
RUN ROOTFS=${img}.tar.zst && \
    sha256sum -c ${img}.tar.zst.SHA256 && \
    mkdir /rootfs && \
    tar -C /rootfs --extract --file "${ROOTFS}"

FROM scratch AS root
ARG img=base-devel
ARG version=VERSION
ARG githash=REVISION
ARG created=CREATED
LABEL org.opencontainers.image.title="Artix Linux ${img} Image"
LABEL org.opencontainers.image.description="Official containerd image of Artix Linux, a fork of Arch Linux that respects init freedom."
LABEL org.opencontainers.image.vendor="Artix Linux"
LABEL org.opencontainers.image.authors="Cory Sanin <corysanin@artixlinux.org>, artoo <artoo@artixlinux.org>, Chris Cromer <cromer@artixlinux.org>"
LABEL org.opencontainers.image.url="https://gitea.artixlinux.org/artixdocker"
LABEL org.opencontainers.image.documentation="https://wiki.artixlinux.org/"
LABEL org.opencontainers.image.source="https://gitea.artixlinux.org/artixdocker/artixlinux-docker"
LABEL org.opencontainers.image.licenses="GPL-3.0-or-later"
LABEL org.opencontainers.image.version="${version}"
LABEL org.opencontainers.image.revision="${githash}"
LABEL org.opencontainers.image.created="${created}"

COPY --from=build /rootfs/ /

RUN ldconfig && \
    sed -i "/BUILD_ID/a VERSION_ID=${version}" /etc/os-release

ENV LANG=C.UTF-8
CMD ["/usr/bin/bash"]