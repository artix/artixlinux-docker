DOCKER = docker
OWNER = artixdocker
REGISTRY = gitea.artixlinux.org
BUILDDIR = $(shell pwd)/build
OUTPUTDIR = $(shell pwd)/output

define rootfs
	mkdir -vp $(BUILDDIR)/alpm-hooks/usr/share/libalpm/hooks
	find /usr/share/libalpm/hooks -exec ln -sf /dev/null $(BUILDDIR)/alpm-hooks{} \;

	mkdir -vp $(BUILDDIR)/var/lib/pacman/ $(OUTPUTDIR)
	install -Dm644 /usr/share/artools/pacman.conf.d/galaxy-x86_64.conf $(BUILDDIR)/etc/pacman.conf
	cat pacman-conf.d-noextract.conf >> $(BUILDDIR)/etc/pacman.conf

	fakechroot fakeroot pacman -Sy -r $(BUILDDIR) \
		--noconfirm --dbpath $(BUILDDIR)/var/lib/pacman \
		--config $(BUILDDIR)/etc/pacman.conf \
		--noscriptlet \
		--hookdir $(BUILDDIR)/alpm-hooks/usr/share/libalpm/hooks/ $(2) $(3)

# 	cp --recursive --preserve=timestamps --backup --suffix=.pacnew rootfs/* $(BUILDDIR)/
	rsync -rt --backup --suffix=.pacnew rootfs/* $(BUILDDIR)/

	fakechroot fakeroot chroot $(BUILDDIR) update-ca-trust

	fakechroot fakeroot chroot $(BUILDDIR) sh -c 'pacman-key --init && pacman-key --populate && rm -rf etc/pacman.d/gnupg/{openpgp-revocs.d/,private-keys-v1.d/,pubring.gpg~,gnupg.S.}*'

	fakechroot fakeroot chroot $(BUILDDIR) /usr/bin/esysusers --root "/"

	ln -fs /usr/lib/os-release $(BUILDDIR)/etc/os-release

	# add system users
	#fakechroot fakeroot chroot $(BUILDDIR) /usr/bin/sysusers --root "/"

	# remove passwordless login for root (see CVE-2019-5021 for reference)
	sed -i -e 's/^root::/root:!:/' "$(BUILDDIR)/etc/shadow"

	# fakeroot to map the gid/uid of the builder process to root
	# fixes #22
	fakeroot tar --numeric-owner --xattrs --acls --exclude-from=exclude -C $(BUILDDIR) -c . -f $(OUTPUTDIR)/$(1).tar

	cd $(OUTPUTDIR); zstd --long -T0 -8 $(1).tar; sha256sum $(1).tar.zst > $(1).tar.zst.SHA256
endef

define dockerfile
	sed -e "s|TEMPLATE_ROOTFS_FILE|$(1).tar.zst|" \
	    -e "s|TEMPLATE_ROOTFS_RELEASE_URL|Local build|" \
	    -e "s|TEMPLATE_ROOTFS_DOWNLOAD|ROOTFS=\"$(1).tar.zst\"|" \
	    -e "s|TEMPLATE_ROOTFS_HASH|$$(cat $(OUTPUTDIR)/$(1).tar.zst.SHA256)|" \
	    -e "s|TEMPLATE_VERSION_ID|dev|" \
	    Dockerfile.template > $(OUTPUTDIR)/Dockerfile.$(1)
endef

.PHONY: clean
clean:
	rm -rf $(BUILDDIR) $(OUTPUTDIR)

$(OUTPUTDIR)/base.tar.zst:
	$(call rootfs,base,base)

$(OUTPUTDIR)/base-openrc.tar.zst:
	$(call rootfs,base-openrc,base,elogind-openrc)

$(OUTPUTDIR)/base-runit.tar.zst:
	$(call rootfs,base-runit,base,elogind-runit)

$(OUTPUTDIR)/base-s6.tar.zst:
	$(call rootfs,base-s6,base,elogind-s6)

$(OUTPUTDIR)/base-dinit.tar.zst:
	$(call rootfs,base-dinit,base,elogind-dinit)

$(OUTPUTDIR)/Dockerfile.base: $(OUTPUTDIR)/base.tar.zst
	$(call dockerfile,base)

$(OUTPUTDIR)/Dockerfile.base-openrc: $(OUTPUTDIR)/base-openrc.tar.zst
	$(call dockerfile,base-openrc)

$(OUTPUTDIR)/Dockerfile.base-runit: $(OUTPUTDIR)/base-runit.tar.zst
	$(call dockerfile,base-runit)

$(OUTPUTDIR)/Dockerfile.base-s6: $(OUTPUTDIR)/base-s6.tar.zst
	$(call dockerfile,base-s6)

$(OUTPUTDIR)/Dockerfile.base-dinit: $(OUTPUTDIR)/base-dinit.tar.zst
	$(call dockerfile,base-dinit)

$(OUTPUTDIR)/base-devel.tar.zst:
	$(call rootfs,base-devel,base-devel)

$(OUTPUTDIR)/Dockerfile.base-devel: $(OUTPUTDIR)/base-devel.tar.zst
	$(call dockerfile,base-devel)

.PHONY: docker-file-base-openrc
dockerfile-base-openrc: $(OUTPUTDIR)/Dockerfile.base-openrc

.PHONY: docker-file-base-runit
dockerfile-base-runit: $(OUTPUTDIR)/Dockerfile.base-runit

.PHONY: docker-file-base-s6
dockerfile-base-s6: $(OUTPUTDIR)/Dockerfile.base-s6

.PHONY: docker-file-base-dinit
dockerfile-base-dinit: $(OUTPUTDIR)/Dockerfile.base-dinit

.PHONY: docker-file-base
dockerfile-base: $(OUTPUTDIR)/Dockerfile.base

.PHONY: docker-file-base-devel
dockerfile-base-devel: $(OUTPUTDIR)/Dockerfile.base-devel

.PHONY: docker-image-base-openrc
image-base-openrc: $(OUTPUTDIR)/Dockerfile.base-openrc
	${DOCKER} build -f $(OUTPUTDIR)/Dockerfile.base-openrc -t $(REGISTRY)/$(OWNER)/artixlinux:base-openrc $(OUTPUTDIR)

.PHONY: docker-image-base-runit
image-base-runit: $(OUTPUTDIR)/Dockerfile.base-runit
	${DOCKER} build -f $(OUTPUTDIR)/Dockerfile.base-runit -t $(REGISTRY)/$(OWNER)/artixlinux:base-runit $(OUTPUTDIR)

.PHONY: docker-image-base-s6
image-base-s6: $(OUTPUTDIR)/Dockerfile.base-s6
	${DOCKER} build -f $(OUTPUTDIR)/Dockerfile.base-s6 -t $(REGISTRY)/$(OWNER)/artixlinux:base-s6 $(OUTPUTDIR)

.PHONY: docker-image-base-dinit
image-base-dinit: $(OUTPUTDIR)/Dockerfile.base-dinit
	${DOCKER} build -f $(OUTPUTDIR)/Dockerfile.base-dinit -t $(REGISTRY)/$(OWNER)/artixlinux:base-dinit $(OUTPUTDIR)

.PHONY: docker-image-base
image-base: $(OUTPUTDIR)/Dockerfile.base
	${DOCKER} build -f $(OUTPUTDIR)/Dockerfile.base -t $(REGISTRY)/$(OWNER)/artixlinux:base $(OUTPUTDIR)

.PHONY: docker-image-base-devel
image-base-devel: $(OUTPUTDIR)/Dockerfile.base-devel
	${DOCKER} build -f $(OUTPUTDIR)/Dockerfile.base-devel -t $(REGISTRY)/$(OWNER)/artixlinux:base-devel $(OUTPUTDIR)
