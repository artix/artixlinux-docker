# Docker Base Image for Artix Linux 

This repository contains all scripts and files needed to create a Docker images for the Artix Linux distribution.

## Dependencies

Install the following Artix Linux packages:
* make
* fakechroot
* docker
* fakeroot
* artools

## Usage

Run `make image-base-devel` to build the base devel image.

Run `make image-base-openrc` to build the openrc base image.

Run `make image-base-runit` to build the runit base image.

Run `make image-base-s6` to build the s6 base image.

Run `make image-base-dinit` to build the dinit base image.


Alternatively:

Run `make dockerfile-base-devel` to build the base devel dockerfile.

Run `make dockerfile-base-openrc` to build the openrc base dockerfile.

Run `make dockerfile-base-runit` to build the runit base dockerfile.

Run `make dockerfile-base-s6` to build the s6 base dockerfile.

Run `make dockerfile-base-dinit` to build the dinit base dockerfile.

## Purpose

* Provide Artix Linux in a Docker Image
* Provide the most simple but complete image to base every other upon
* `pacman` needs to work out of the box
* All installed packages have to be kept unmodified
